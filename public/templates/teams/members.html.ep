% layout 'admin';
% use Mojo::ByteStream 'b';

<div class="container">

    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-8 col-md-8 col-xs-8">
            
            <a class="btn btn-default" href="<%=$back_url%>"><span class="glyphicon glyphicon-arrow-left"></span> Back</a>
            <a class="btn btn-info" href="/teams"><span class="glyphicon glyphicon-arrow-left"></span> Back to the list of all teams</a>

            % if(team_can_be_deleted($teamid) and is_manager()){
                <a class="btn btn-danger" href="/teams/delete/<%=$teamid%>" data-toggle="tooltip" data-placement="left" title="Delete team"><span class="glyphicon glyphicon-trash"></span> Delete team</a>
            % }
            % else{
                <a class="btn btn-default" href="#" data-toggle="tooltip" data-placement="left" title="Cannot delete team because it still has members"><span class="glyphicon glyphicon-trash"></span> <del>Delete team</del></a>
            % }

            <div class="btn-group">
                <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-cog"></span>
                    <span>Service functions</span>
                    <span class="caret"></span>
                </button>   
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="/teams/unrealted_papers/<%=$teamid%>" data-toggle="tooltip" data-placement="left" title="Click to see the unrelated publications"><span class="glyphicon glyphicon-search"></span> Show completly unrealted papers...</a>
                    </li>
                    % if(is_admin()){
                        <li>
                            <a href="/teams/delete/<%=$teamid%>/force<%=backurl%>" data-toggle="tooltip" data-placement="left" title="Delete this team ignoring any memberships"><span class="glyphicon glyphicon-trash" style="color: red;"></span> Delete this team with force!</a>
                        </li> 
                    % }
                </ul>
            </div>
        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-8 col-md-8 col-xs-8">
            
            <a class="btn btn-default btn-sm" href="/publications?team=<%= $teamname %>"  data-toggle="tooltip" data-placement="left" title="Show papers of the team <%= $teamname %>"><i class="fa fa-users"></i><span class="glyphicon glyphicon-list-alt"></span> Show papers</a>

            <a class="btn btn-default btn-sm" href="/landing/publications?team=<%= $teamname %>"  data-toggle="tooltip" data-placement="left" title="Landing page with papers of the team <%= $teamname %>"><i class="fa fa-users"></i><span class="glyphicon glyphicon-plane"></span></a>
            
            <a class="btn btn-default btn-sm" href="/landing-years/publications?team=<%= $teamname %>" data-toggle="tooltip" data-placement="left" title="Landing page (years) with papers of the team <%= $teamname %>"><i class="fa fa-users"></i><span class="glyphicon glyphicon-plane"></span><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></a>

            <a class="btn btn-default btn-sm" href="/read/bibtex?team=<%= $teamname %>" data-toggle="tooltip" data-placement="left" title="Get bibtex of all papers of team <%= $teamname %>"><i class="fa fa-users"></i><span class="glyphicon glyphicon-plane"></span> BibTeX</a>
            
        </div>
    </div>

    <hr>    
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-6 col-md-6 col-xs-6">
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="fa  fa-exclamation-circle "></i> There are the members of team <strong><%= $teamname %></strong>
            </div>
        </div>
    </div>
  

    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-12 col-md-12 col-xs-12">
            <table class="table table-striped table-condensed">
            <thead>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
            % my $i = 0;
            % foreach my $author_id (@{$members}){
                % my $start = ${$start_arr}[$i];
                % my $stop = ${$stop_arr}[$i];
                <tr>
                    <td>    
                        <a class="btn btn-danger btn-sm" href="/authors/<%=$author_id%>/remove_from_team/<%=$teamid%><%=backurl()%>" data-toggle="tooltip" data-placement="right" title="Remove author <%= get_master_for_id($author_id) %> from team <%= $teamname %>"><i class="fa fa-users"></i> <span class="glyphicon glyphicon-minus"></span></a>
                    </td>
                    <td>
                        <div class="btn-group">
                            % my $visibility = author_is_visible($author_id) || 0;
                            % if ($visibility == 1){
                                <a class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="Make author invisible" href="/authors/toggle_visibility/<%=$author_id%><%=backurl($self)%>"><span class="glyphicon glyphicon-eye-open"></span></a>
                            %}
                            %else{
                                <a class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="left" title="Make author visible" href="/authors/toggle_visibility/<%=$author_id%><%=backurl($self)%>"><span class="glyphicon glyphicon-eye-close"></span></a>
                            %}
                            <a class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="left" title="Show author's edit page" href="/authors/edit/<%=$author_id%><%=backurl($self)%>"><span class="glyphicon glyphicon-user"></span> <span class="glyphicon glyphicon-pencil"></span></a>

                            <span class="btn btn-default btn-sm"  data-toggle="tooltip" data-placement="left" title="Author's master name and ID"><span class="glyphicon glyphicon-user"></span> <%= get_master_for_id($author_id) %> ( <span class="glyphicon glyphicon-barcode"></span> <%= $author_id %> )</span>
                        </div>
                    </td>
                    <td>
                        <span class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="left" title="Year of becoming a member of <%= $teamname %>"><i class="fa fa-sun-o"></i> <%= $start %></span>
                    </td>
                    <td>
                        <span class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="left" title="Year of leaving <%= $teamname %>"><i class="fa fa-moon-o"></i> <%= $stop %></span>
                    </td>
                    <td>
                        <a class="btn btn-default btn-sm" href="/landing/publications?team=<%= $teamname %>&author=<%= get_master_for_id($author_id) %>"  data-toggle="tooltip" data-placement="left" title="Landing page with papers of <%= get_master_for_id($author_id) %> as he was a member of <%= $teamname %>"><span class="glyphicon glyphicon-plane"></span></a>
                        <a class="btn btn-default btn-sm" href="/landing-years/publications?team=<%= $teamname %>&author=<%= get_master_for_id($author_id) %>" data-toggle="tooltip" data-placement="left" title="Landing page (years) with papers of <%= get_master_for_id($author_id) %> as he was a member of <%= $teamname %>"><span class="glyphicon glyphicon-plane"></span><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></a>
                    </td>
                    % $i++;
                </tr>
            %}
            </tbody>
            </table>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-6 col-md-6 col-xs-6">
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="fa  fa-exclamation-circle "></i> To add a new author to team <strong><%= $teamname %></strong>, go to that author's page.
            </div>
        </div>
    </div>
      
</div>

